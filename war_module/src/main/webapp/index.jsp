<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Homework: Maven + Tomcat + Servlet API</title>
</head>
<body>

<form action="first" method="post">
    <h3>Simple calculate form</h3>
    <p>
        <label for="firstNum">First Num</label>
    </p>
    <p>
        <input id="firstNum" name="firstNum" type="text" placeholder="Enter num"/>
    </p>
    <p>
        <label for="secondNum">Second Num</label>
    </p>
    <p>
        <input id="secondNum" name="secondNum" type="text" placeholder="Enter num"/>
    </p>
    <p>
        <label for="operation">Operation </label>
    </p>
    <p>
        <select id="operation" name="operation">
            <option selected="selected" value="1">+</option>
            <option value="2">-</option>
            <option value="3">*</option>
            <option value="4">/</option>
        </select>
    <p>
    <button type="submit"> ADD</button>
</form>
</body>
</html>
