package by.itacademy.servlets;

import by.itacademy.calc.SimpleCalculator;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class FirstServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher redirect = request.getRequestDispatcher("/second");
        redirect.forward(request, response);
        return;
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        SimpleCalculator calculator = new SimpleCalculator();

        int a = Integer.parseInt(request.getParameter("firstNum"));
        int b = Integer.parseInt(request.getParameter("secondNum"));
        int c = Integer.parseInt(request.getParameter("operation"));

        response.getWriter().append("Result is " + calculator.calcRunner(a, b, c));

    }
}
