package by.itacademy.calc;

import org.junit.Test;

import static org.junit.Assert.*;

public class SimpleCalculatorTest {

    @Test
    public void summation() {
        int a = 5;
        int b = 6;

        SimpleCalculator calculator = new SimpleCalculator();

        int sum = (int) calculator.calcRunner(5, 6, 1);

        assertEquals("Expected valid operation of summation", 11, sum);
    }

    @Test
    public void multiply() {
        int a = 5;
        int b = 6;

        SimpleCalculator calculator = new SimpleCalculator();

        int mult = (int) calculator.calcRunner(5, 6, 3);

        assertEquals("Expected valid operation of multiply", 30, mult);
    }

    @Test
    public void division() {
        int a = 5;
        int b = 6;

        SimpleCalculator calculator = new SimpleCalculator();

        float div = calculator.calcRunner(5, 6, 4);

        assertEquals("Expected valid operation of division", (float) 0.8333333, div, 0.00000001);
    }

    @Test
    public void subtraction() {
        int a = 5;
        int b = 6;

        SimpleCalculator calculator = new SimpleCalculator();

        int subtraction = (int) calculator.calcRunner(5, 6, 2);

        assertEquals("Expected valid operation of multiply", -1, subtraction);
    }
}