package by.itacademy.calc;

public class SimpleCalculator {
    private int summation(int a, int b) {
        return a + b;
    }

    private int multiply(int a, int b) {
        return a * b;
    }

    private float division(int a, int b) {
        return (float) a / b;
    }

    private int subtraction(int a, int b) {
        return a - b;
    }

    public float calcRunner(int a, int b, int c) {

        switch (c) {
            case 1:
                return summation(a, b);
            case 2:
                return subtraction(a, b);
            case 3:
                return multiply(a, b);
            case 4:
                return division(a, b);

        }
        return 9;

    }
}
